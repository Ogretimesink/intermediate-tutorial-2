/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.h
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#ifndef __TutorialApplication_h_
#define __TutorialApplication_h_

#include <Terrain/OgreTerrain.h>
#include <Terrain/OgreTerrainGroup.h>

#include "BaseApplication.h"

//---------------------------------------------------------------------------

class TutorialApplication : public BaseApplication
{
public:
	// C++ application constructor
	TutorialApplication(void);

	// C++ application destructor
	virtual ~TutorialApplication(void);

protected:
	// Function to fill the scene with graphic objects
	virtual void createScene(void);

	// Function to initialize screen overlay objects
	virtual void createFrameListener(void);

	// Function to delete scene objects
	virtual void destroyScene();

	// Function to update the scene every frame
	virtual bool frameRenderingQueued(const Ogre::FrameEvent& fe);

private:
	// Function to process keyboard input
	virtual bool keyPressed(const OIS::KeyEvent& ke);

	// Function to process keyboard input
	virtual bool keyReleased(const OIS::KeyEvent& ke);
 
	// Function to process mouse input
	virtual bool mouseMoved(const OIS::MouseEvent& me);

	// Function to process mouse input
	virtual bool mousePressed(const OIS::MouseEvent& me, OIS::MouseButtonID id);

	// Function to process mouse input
	virtual bool mouseReleased(const OIS::MouseEvent& me, OIS::MouseButtonID id);

	// Get the height map that will make the terrainh
	void getTerrainImage(bool flipX, bool flipY, Ogre::Image& img);

	// Load or define new terrain data files
	void defineTerrain(long x, long y);

	// Create the blendmaps that make up the terrain
	void initBlendMaps(Ogre::Terrain* terrain);

	// Define the terrain and its texture layers
	void configureTerrainDefaults(Ogre::Light* light);
 
	// Terrain loaded flag
	bool mTerrainsImported;

	// Object that manages the terrain
	Ogre::TerrainGroup* mTerrainGroup;

	// Object that holds all the terrain data
	Ogre::TerrainGlobalOptions* mTerrainGlobals;

	// On screen message lable
	OgreBites::Label* mInfoLabel;

	// Function to check that the camera doesn't pass through the terrain
	void handleCameraCollision();

	// Speed at which the camera moves by mouse input
	float mRotSpd;

	// Mouse button pressed flags
	bool mLMouseDown, mRMouseDown;

	// Object to which to attach and display computer graphic models in the scene
	Ogre::SceneNode* mCurObject;
};

//---------------------------------------------------------------------------

#endif // #ifndef __TutorialApplication_h_

//---------------------------------------------------------------------------
