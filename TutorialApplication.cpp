/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
	: mTerrainGroup(0)
	, mTerrainGlobals(0)
	, mInfoLabel(0)
	, mRotSpd(0.1f)
	, mLMouseDown(false)
	, mRMouseDown(false)
	, mCurObject(0)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
	// Position the camera
	mCamera->setPosition(Ogre::Vector3(1683, 50, 2116));

	// Look at the position with the camera
	mCamera->lookAt(Ogre::Vector3(1963, 50, 1660));

	// Distance the camera won't render a mesh
	mCamera->setNearClipDistance(0.1f);

	// Check if current render system has infinite far clip distance capability
	bool infiniteClip =
		mRoot->getRenderSystem()->getCapabilities()->hasCapability(
		Ogre::RSC_INFINITE_FAR_PLANE);
 
	// Check the result of the infinite far clip distance capability and set
	if (infiniteClip)
		// Has infinite far clip distance capability so no far clipping
		mCamera->setFarClipDistance(0);
	else
		// Does not have infinite far clip distance capability so set to 50000
		mCamera->setFarClipDistance(50000);

	// Add light which illuminates all objects in the scene regardless of direction
	mSceneMgr->setAmbientLight(Ogre::ColourValue(0.2f, 0.2f, 0.2f));
 
	// Direction of the light 
	Ogre::Vector3 lightdir(0.55f, -0.3f, 0.75f);

	// Normalize the vector to a length of 1
	lightdir.normalise();

	// Create a directional light that gives shadows
	Ogre::Light* light = mSceneMgr->createLight("TestLight");

	// Set the light type to directinal
	light->setType(Ogre::Light::LT_DIRECTIONAL);

	// Set the direction
	light->setDirection(lightdir);

	// Color of light that reflects off scene models
	light->setDiffuseColour(Ogre::ColourValue::White);

	// Shininess of light that reflects off scene models
	light->setSpecularColour(Ogre::ColourValue(0.4f, 0.4f, 0.4f));

	// Declare and initialize a color
	Ogre::ColourValue fadeColour(0.9f, 0.9f, 0.9f);

	// Create a linear fog
	mSceneMgr->setFog(Ogre::FOG_LINEAR, fadeColour, 0, 600, 9000);

	// Make the scene sky with a skydome
	mSceneMgr->setSkyDome(true, "Examples/CloudySky", 5, 8);

	// Make terrain class to hold all terrain information
	mTerrainGlobals = OGRE_NEW Ogre::TerrainGlobalOptions();

	// Construct terrain object that manages terrains
	mTerrainGroup = OGRE_NEW Ogre::TerrainGroup(
		mSceneMgr, 
		Ogre::Terrain::ALIGN_X_Z, 
		513, 
		12000.0);

	// Set naming convention for terrains that will be constructed then saved to disk
	mTerrainGroup->setFilenameConvention(Ogre::String("terrain"), Ogre::String("dat"));

	// Set the location of the middle of the terrain
	mTerrainGroup->setOrigin(Ogre::Vector3::ZERO);

	// Configure global options
	configureTerrainDefaults(light);

	// Construct the scene terrain
	for (long x = 0; x <= 0; ++x)
	{
		for (long y = 0; y <= 0; ++y)
		{
			// Load the terrain from data file or construct from heightmap image
			defineTerrain(x, y);
		}
	}

	// Load terrains
	mTerrainGroup->loadAllTerrains(true);

	// Set blend maps for terrains
	if (mTerrainsImported)
	{
		// Declare counter to iterate between all the terrains
		Ogre::TerrainGroup::TerrainIterator ti = mTerrainGroup->getTerrainIterator();

		// Iterate through all the terrains
		while(ti.hasMoreElements())
		{
			// Get the terrain instance
			Ogre::Terrain* t = ti.getNext()->instance;

			// Create the texture layers for the terrain instance
			initBlendMaps(t);
		}
	}

	// Clean up resources no longer used
	mTerrainGroup->freeTemporaryResources();
}
//---------------------------------------------------------------------------
void TutorialApplication::createFrameListener(void)
{
	// Call the BaseApplication virtual function because we still want its functionality
	BaseApplication::createFrameListener();

	// Create on screen label for displaying update messages
	mInfoLabel = mTrayMgr->createLabel(OgreBites::TL_TOP, "TerrainInfo", "", 350);

	// Show the screen cursor by default
	mTrayMgr->showCursor();
}
//---------------------------------------------------------------------------
void TutorialApplication::destroyScene(void)
{
	// Destroy the object that manages the terrain
	OGRE_DELETE mTerrainGroup;

	// Destroy the object that holds all terrain data
	OGRE_DELETE mTerrainGlobals;
}
//---------------------------------------------------------------------------
bool TutorialApplication::frameRenderingQueued(const Ogre::FrameEvent& fe)
{
	// Call the BaseApplication virtual function because we still want its functionality
	bool ret = BaseApplication::frameRenderingQueued(fe);

	// Determine if the terrains are being constructed
	if (mTerrainGroup->isDerivedDataUpdateInProgress())
	{
		// Move the screen label to a visible visual tray
		mTrayMgr->moveWidgetToTray(mInfoLabel, OgreBites::TL_TOP, 0);

		// Display the screen label to display updating message
		mInfoLabel->show();

		// Terrain loaded flag
		if (mTerrainsImported)
		{
			// Terrain is being constructed
			mInfoLabel->setCaption("Building terrain...");
		}
		else
		{
			// Terrain is being updated after a change
			mInfoLabel->setCaption("Updating terrain...");
		}
	}
	else
	{
		// Move the screen label to a non-visible tray
		mTrayMgr->removeWidgetFromTray(mInfoLabel);

		// Hide the screen lable
		mInfoLabel->hide();

		// Terrain loaded flag
		if (mTerrainsImported)
		{
			// Save the terrain data to disk
			mTerrainGroup->saveAllTerrains(true);

			// Terrain loaded flag
			mTerrainsImported = false;
		}
	}

	// Check that the camera doesn't pass through the terrain
	handleCameraCollision();

	return ret;
}
//---------------------------------------------------------------------------
void TutorialApplication::getTerrainImage(bool flipX, bool flipY, Ogre::Image& img)
{
	// Load the heightmap image
	img.load("terrain.png", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);

	// Determine the heightmap is aligned on Y axis
	if (flipX)
	{
		// Flip the image on the Y axis
		img.flipAroundY();
	}

	// Determine the heightmap is aligned on X axis
	if (flipY)
	{
		// Flip the image on the X axis
		img.flipAroundX();
	}
}
//---------------------------------------------------------------------------
void TutorialApplication::defineTerrain(long x, long y)
{
	// Determine a file name for the specific terrain at x and y
	Ogre::String filename = mTerrainGroup->generateFilename(x, y);

	// Determine if a file with the name exists
	bool exists =
		Ogre::ResourceGroupManager::getSingleton().resourceExists(
			mTerrainGroup->getResourceGroup(),
			filename);

	// If the file does or does not exist
	if (exists)
	{
		// Load as a terrain if file exists
		mTerrainGroup->defineTerrain(x, y);
	}
	else
	{
		// Declare a new heightmap image 
		Ogre::Image img;

		// Load a heightmap image to make the terrain
		getTerrainImage(x % 2 != 0, y % 2 != 0, img);

		// Construct the terrain from the heightmap image
		mTerrainGroup->defineTerrain(x, y, &img); 

		// Set the loaded terrain flag
		mTerrainsImported = true;
	}
}
//---------------------------------------------------------------------------
void TutorialApplication::initBlendMaps(Ogre::Terrain* terrain)
{
	// Height at which the weird fungus layer will stop
	Ogre::Real minHeight0 = 50;

	// Height at which weird fungus layer will stopr
	Ogre::Real fadeDist0 = 100;

	// Height at which the rocky layer will stop
	Ogre::Real minHeight1 = 200;

	// Height at which rocky layer will fade will stop
	Ogre::Real fadeDist1 = 280;

	// Get the first blendmap
	Ogre::TerrainLayerBlendMap* blendMap0 = terrain->getLayerBlendMap(1);

	// Get the second blendmap
	Ogre::TerrainLayerBlendMap* blendMap1 = terrain->getLayerBlendMap(2);
 
	// Get the pointer to the first blend map
	float* pBlend0 = blendMap0->getBlendPointer();

	// Get the pointer to the second blend map
	float* pBlend1 = blendMap1->getBlendPointer();

	// Loop through the y coordinates of the terrain size
	for (Ogre::uint16 y = 0; y < terrain->getLayerBlendMapSize(); ++y)
	{
		// Loop through the x coordinates of the terrain size
		for (Ogre::uint16 x = 0; x < terrain->getLayerBlendMapSize(); ++x)
		{
			// Declare terrain space coordinates
			Ogre::Real tx, ty;
 
			// Convert image space to terrain space
			blendMap0->convertImageToTerrainSpace(x, y, &tx, &ty);

			// Find the height at the x and y coordinate
			Ogre::Real height = terrain->getHeightAtTerrainPosition(tx, ty);

			// Calculate first blend map layer
			Ogre::Real val = (height - minHeight0) / fadeDist0;

			// Limit the value to between 0 and 1
			val = Ogre::Math::Clamp(val, (Ogre::Real)0, (Ogre::Real)1);
			*pBlend0++ = val;
 
			// Calculate second blend map layer
			val = (height - minHeight1) / fadeDist1;

			// Limit the value to between 0 and 1
			val = Ogre::Math::Clamp(val, (Ogre::Real)0, (Ogre::Real)1);
			*pBlend1++ = val;
		}
	}
 
	// First blend map needs updating
	blendMap0->dirty();

	// Second blend map needs updating
	blendMap1->dirty();

	// Make changes to first blend map
	blendMap0->update();

	// Make changes to second blend map
	blendMap1->update();
}
//---------------------------------------------------------------------------
void TutorialApplication::configureTerrainDefaults(Ogre::Light* light)
{
	// Precision of terrain (lower the better)
	mTerrainGlobals->setMaxPixelError(8);

	// Distance to render the light mapped terrain
	mTerrainGlobals->setCompositeMapDistance(3000);
 
	// Set the direction of light and shadow
	mTerrainGlobals->setLightMapDirection(light->getDerivedDirection());

	// Set the ambient light which illuminates all objects in the scene regardless of direction
	mTerrainGlobals->setCompositeMapAmbient(mSceneMgr->getAmbientLight());

	// Set the light that reflects off scene models
	mTerrainGlobals->setCompositeMapDiffuse(light->getDiffuseColour());

	// Configure default import settings for if we use imported image
	Ogre::Terrain::ImportData& importData = mTerrainGroup->getDefaultImportSettings();

	// Resolution of terrain in accordance to world size
	importData.terrainSize = 513;

	// Size in Ogre units of the terrain
	importData.worldSize = 12000.0;

	// Factor by which to scale points in the terrain height map
	importData.inputScale = 600;

	// Minimum size of tile that the terrain will be subdivided into
	importData.minBatchSize = 33;

	// Maximum size of tile that the terrain will be subdivided into
	importData.maxBatchSize = 65;

	// Number of terrain texture layers
	importData.layerList.resize(3);

	// Size of tile of first texture layer splat
	importData.layerList[0].worldSize = 30;

	// Set terrain first layer texture
	importData.layerList[0].textureNames.push_back("grass_green-01_diffusespecular.dds");

	// Set terrain first layer texture height
	importData.layerList[0].textureNames.push_back("grass_green-01_normalheight.dds");

	// Size of tile of second texture layer splat
	importData.layerList[1].worldSize = 200;

	// Set terrain second layer texture
	importData.layerList[1].textureNames.push_back("growth_weirdfungus-03_diffusespecular.dds");

	// Set terrain second layer texture height
	importData.layerList[1].textureNames.push_back("growth_weirdfungus-03_normalheight.dds");

	// Size of tile of last texture layer splat
	importData.layerList[2].worldSize = 100;

	// Set terrain last layer texture
	importData.layerList[2].textureNames.push_back("dirt_grayrocky_diffusespecular.dds");

	// Set terrain last layer texture height
	importData.layerList[2].textureNames.push_back("dirt_grayrocky_normalheight.dds");
}
//---------------------------------------------------------------------------
bool TutorialApplication::keyPressed(const OIS::KeyEvent& ke)
{
	// Call the BaseApplication virtual function because we still want its functionality
	BaseApplication::keyPressed(ke);

	return true;
}
//---------------------------------------------------------------------------
bool TutorialApplication::keyReleased(const OIS::KeyEvent& ke)
{
	// Call the BaseApplication virtual function because we still want its functionality
	BaseApplication::keyReleased(ke);

	return true;
}
//---------------------------------------------------------------------------
bool TutorialApplication::mouseMoved(const OIS::MouseEvent& me)
{
	// Send mouse moved to the tray manager that controls the screen cursor movement
	mTrayMgr->injectMouseMove(me);

	// Check the left mouse button press flag
	if (mLMouseDown)
	{
		// Make a scene ray starting at the mouse location
		Ogre::Ray MouseRay = mTrayMgr->getCursorRay(mCamera);

		// Query the scene terrain with the scene ray
		Ogre::TerrainGroup::RayResult result = mTerrainGroup->rayIntersects(MouseRay);

		// Check if the ray intersects with the terrain
		if (result.terrain)
		{
			// Move the scene node object to the location of the terrain intersection
			mCurObject->setPosition(result.position);
		}
	}
	// Check the right mouse button press flag
	else if (mRMouseDown)
	{
		// Rotate the camera right or left
		mCamera->yaw(Ogre::Degree(-me.state.X.rel * mRotSpd));

		// Move the camera up or down
		mCamera->pitch(Ogre::Degree(-me.state.Y.rel * mRotSpd));
	}

	return true;
}
//---------------------------------------------------------------------------
bool TutorialApplication::mousePressed(const OIS::MouseEvent& me, OIS::MouseButtonID id)
{
	// Process the mouse button pressed input
	switch (id)
	{
		// The left mouse button
		case OIS::MB_Left:
			{
				// Set the left mouse button press flag
				mLMouseDown = true;

				// Make a scene ray starting at the mouse location
				Ogre::Ray MouseRay = mTrayMgr->getCursorRay(mCamera);

				// Query the scene terrain with the scene ray
				Ogre::TerrainGroup::RayResult result = mTerrainGroup->rayIntersects(MouseRay);

				// Check if the ray intersects with the terrain
				if (result.terrain)
				{
					// Create an instance of a computer graphic model robot mesh
					Ogre::Entity* ent = mSceneMgr->createEntity("robot.mesh");

					// Create a new object in the scene
					mCurObject = mSceneMgr->getRootSceneNode()->createChildSceneNode();

					// Set the new object to the location of the terrain intersection
					mCurObject->setPosition(result.position);

					// Set the size of the object
					mCurObject->setScale(0.1f, 0.1f, 0.1f);

					// Add the entity to the node to display the mesh in the scene
					mCurObject->attachObject(ent);
				}

				// Finish left mouse button processing
				break;
			}
		// The right mouse button
		case OIS::MB_Right:
			{
				// Set the right mouse button press flag
				mRMouseDown = true;

				// Hide the screen cursor while moving the camera with the mouse
				mTrayMgr->hideCursor();

				// Finish right mouse button processing
				break;
			}
	}

	return true;
}
//---------------------------------------------------------------------------
bool TutorialApplication::mouseReleased(const OIS::MouseEvent& me, OIS::MouseButtonID id)
{
	// Process the mouse button released input
	switch (id)
	{
		// The left mouse button
		case OIS::MB_Left:
			{
				// Set the left mouse button press flag
				mLMouseDown = false;

				// Finish left mouse button processing
				break;
			}
		// The right mouse button
		case OIS::MB_Right:
			{
				// Set the right mouse button press flag
				mRMouseDown = false;

				// Show the screen cursor after moving the camera with the mouse
				mTrayMgr->showCursor();

				// Finish right mouse button processing
				break;
			}
	}

	return true;
}
//---------------------------------------------------------------------------
void TutorialApplication::handleCameraCollision()
{
	// Get the position of the camera
	Ogre::Vector3 camPos = mCamera->getPosition();

	// Make a scene ray starting at the camera location and going down
	Ogre::Ray camRay(Ogre::Vector3(camPos.x, 5000.0, camPos.z), Ogre::Vector3::NEGATIVE_UNIT_Y);

	// Query the scene terrain with the scene ray
	Ogre::TerrainGroup::RayResult result = mTerrainGroup->rayIntersects(camRay);
 
	// Check if the ray intersects with the terrain
	if (result.terrain)
	{
		// Get the height position of the terrain intersect
		Ogre::Real terrainHeight = result.position.y;
 
		// Check if the height of the terrain at intersect plus ten is higher than the camera height
		if (camPos.y < (terrainHeight + 10.0))
		{
			// Set the camera height to the terrain height plus ten
			mCamera->setPosition(camPos.x, terrainHeight + 10.0, camPos.z);
		}
	}
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
	int main(int argc, char *argv[])
#endif
	{
		// Create application object
		TutorialApplication app;

		try {
			app.go();
		} catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
			std::cerr << "An exception has occurred: " <<
			e.getFullDescription().c_str() << std::endl;
#endif
		}

		return 0;
	}

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------